FROM openjdk:11.0.10-jdk

ARG VERSION='unknown'

COPY . /gatling-gradle
WORKDIR /gatling-gradle
RUN ./gradlew clean compileGatlingScala compileTestScala
RUN echo "Performance test version: $VERSION" > /version.txt

ENTRYPOINT ./entrypoint.sh