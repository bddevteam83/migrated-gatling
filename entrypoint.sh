#!/bin/bash -xe

export GATLING_LOG_LEVEL=${GATLING_LOG_LEVEL:-'INFO'}
export GATLING_RUN_CMD=${GATLING_RUN_CMD:-'gatlingRun'}

cat /version.txt

./gradlew ${GATLING_RUN_CMD}
