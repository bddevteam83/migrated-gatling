### cleanup

```bash
killall kubectl
ls *.yaml | xargs -I{} kubectl delete -f {}
```

### deploy

```bash
ls configmap.*.yaml | xargs -I{} kubectl apply -f {}
ls deploy.*.yaml | xargs -I{} kubectl apply -f {}
ls service.*.yaml | xargs -I{} kubectl apply -f {}
```

### wait for all pods

```bash
watch kubectl get pods
```

### port forward

```bash
kubectl port-forward service/grafana 3111:3000 &
```

### browse grafana

admin:********

```bash
open http://localhost:3111
```

### run gatling job

```bash
ls job.*.yaml | xargs -I{} kubectl delete -f {}
ls job.*.yaml | xargs -I{} kubectl apply -f {}
```