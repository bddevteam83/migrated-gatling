# Gatling with Gradle
Gatling with Gradle, InfluxDB v2.0 with Telegraf and Grafana for reporting.   

## run environment
```
cd env
docker-compose up
```

## run all simulations
```
export GATLING_LOG_LEVEL=INFO
export GATLING_RAMP_USERS=111
export GATLING_RAMP_SECONDS=30
export GATLING_MAX_DURATION_SECONDS=90
 
 ./gradlew clean gatlingRun
```

## run one simulation only

```
./gradlew clean gatlingRun-load.PongSimulation
```

## integration (flo-qa)

```shell
( GATLING_LOG_LEVEL=INFO GATLING_FLORATER_ACCOUNT_SERVICE_URL=http://10.0.211.168:8440 GATLING_FLORATER_ENGINE_SERVICE_URL=http://10.0.211.167:8442 GATLING_RAMP_USERS=2 GATLING_RAMP_SECONDS=2 GATLING_MAX_DURATION_SECONDS=30 ./gradlew clean gatlingRun-load.SendEvents2Simulation)

```

## integration (solid)

```shell
( GATLING_LOG_LEVEL=INFO GATLING_FLORATER_ACCOUNT_SERVICE_URL=http://10.0.10.139:80 GATLING_FLORATER_ENGINE_SERVICE_URL=http://10.0.10.129:8442 GATLING_RAMP_USERS=2 GATLING_RAMP_SECONDS=2 GATLING_MAX_DURATION_SECONDS=30 ./gradlew clean gatlingRun-load.SendEvents2Simulation)
```

## influxdb UI

http://localhost:8186/

``` 
user: admin
pass: ********
```

## grafana dashboard

http://localhost:3100/d/nVrrhs1mk/dasboard

```
user: admin
pass: ********
```

![screenshot.png](screenshot.png)

## related Jenkins job

http://jenkins.bdinfra.net/job/gatling/