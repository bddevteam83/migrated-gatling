#!/bin/sh

export USER=`whoami`
export HOST=`hostname`

export DB_HOST=${DB_HOST:-'10.0.9.100'}
export DB_PORT=${DB_PORT:-'5432'}
export DB_NAME=${DB_NAME:-'postgres'}
export DB_USER=${DB_USER:-'postgres'}
export PGPASSWORD=${DB_PASS:-'********'}

export QUERY_INTERVAL_SECONDS=${QUERY_INTERVAL_SECONDS:-'10'}

env|sort

function send_data(){
    local data=$1
    echo "--> $data"
    curl -sw '%{http_code} %{url_effective}\n\n' --request POST "${INFLUX_URL}/api/v2/write?org=${DOCKER_INFLUXDB_INIT_ORG}&bucket=${DOCKER_INFLUXDB_INIT_BUCKET}"  \
         --header "Authorization: Token ${DOCKER_INFLUXDB_INIT_ADMIN_TOKEN}" \
         --data-raw "$data"
}

function db_max_connections(){
  local q="SELECT setting,TO_CHAR(EXTRACT(EPOCH FROM NOW()) * 1000 * 1000 * 1000, '9999999999999999999') FROM pg_settings WHERE name='max_connections';"
    psql -h ${DB_HOST} -p ${DB_PORT} -d ${DB_NAME} -U ${DB_USER}  -A -t -c "${q}" \
    | awk -v DB_HOST=${DB_HOST} \
          -F"|"  \
          '{printf "db_max_connections,host=%s value=%s %s\n",DB_HOST,$1,$2 }'
}

function db_current_connections(){
  local q="SELECT count(*),TO_CHAR(EXTRACT(EPOCH FROM NOW()) * 1000 * 1000 * 1000, '9999999999999999999') FROM pg_stat_activity;"
  psql -h ${DB_HOST} -p ${DB_PORT} -d ${DB_NAME} -U ${DB_USER}  -A -t -c "${q}" \
    | awk -v DB_HOST=${DB_HOST} \
          -F"|"  \
          '{printf "db_current_connections,host=%s value=%s %s\n",DB_HOST,$1,$2 }'
}

function db_app_connections() {
  local q="SELECT datname, count(*),TO_CHAR(EXTRACT(EPOCH FROM NOW()) * 1000 * 1000 * 1000, '9999999999999999999') FROM pg_stat_activity GROUP BY 1;"
  psql -h ${DB_HOST} -p ${DB_PORT} -d ${DB_NAME} -U ${DB_USER}  -A -t -c "${q}" \
    | grep -v '^$' \
    | sed -e 's/^|/null|/'  \
    | awk -v DB_HOST=${DB_HOST} \
          -F"|"  \
          '{printf "db_app_connections,host=%s,app=%s value=%s %s\n",DB_HOST,$1,$2,$3 }'
}

while [ true ]
do
   send_data "$(db_max_connections)"
   send_data "$(db_current_connections)"
   send_data "$(db_app_connections)"

   sleep ${QUERY_INTERVAL_SECONDS}
done