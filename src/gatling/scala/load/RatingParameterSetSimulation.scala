package load

import load.api.{ApiRatingParameterSet, PagedResourceMethods}

class RatingParameterSetSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiRatingParameterSet

}