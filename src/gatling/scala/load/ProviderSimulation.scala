package load

import load.api.{ApiProvider, PagedResourceMethods}

class ProviderSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiProvider

}