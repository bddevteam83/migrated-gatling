package load

import load.api.{ApiEligibilitySetSchema, PagedResourceMethods}

class EligibilitySetSchemaSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiEligibilitySetSchema

}