package load

import io.gatling.core.Predef._
import load.api._
import load.base._

import java.util.UUID
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.{AtomicInteger, AtomicLong}
import scala.concurrent.duration.DurationInt

/**
 * Init data procedure is based on postman collection provided by Heorhii Zhukov
 * https://www.getpostman.com/collections/a034386997b76c5f5ba1
 */
class SendEvents2Simulation extends Simulation with Log with Config with FloRaterProtocol with UtcTimeZone {

  log.info("~~~~~ config: {}", config)

  val sentEventsCount = new AtomicLong(0)
  var subscriberReadyCount = new AtomicInteger(0)

  def allSubscribersAreReady(): Boolean = subscriberReadyCount.get() >= config.loadProfile.rampUsers

  def guid() = UUID.randomUUID().toString

  val feederInit = Iterator.continually {
    Map(
      // Prepare catalog infrastructure
      "serviceGuid" -> guid(),
      "providerGuid" -> guid(),
      "productGuid" -> guid(),
      "calendarGuid" -> guid(),
      "tariffGuid" -> guid(),
      "tariffPlanGuid" -> guid(),
      "tariffPlanRange1Guid" -> guid(),
      "tariffPlanRange2Guid" -> guid(),
      "tariffPlanRange3Guid" -> guid(),
      "balanceGuid" -> guid(),
      "productOfferingGuid" -> guid(),
      "productOfferingPriceGuid" -> guid(),
      // Prepare subscriber infrastructure
      "customerAccountGuid" -> guid(),
      "subscriberGuId" -> guid(),
      "customerGuid" -> guid(),
      "externalTransactionGuId" -> guid(),
      // rating event
      "externalSessionGuId" -> guid()
    )
  }

  val scnPrepareInfrastructureAndSendEvents = scenario("Prepare catalog and subscriber infrastructure")
    .feed(feederInit)
    // Prepare catalog infrastructure
    .exec(ApiService.save(ApiService.saveJson("SMS ${serviceGuid}"), "serviceName"))
    .exec(ApiProvider.save(ApiProvider.saveJson("Provider ${providerGuid}", "${serviceGuid}"), "serviceId"))
    .exec(ApiProduct.save(ApiProduct.saveJson("Product ${productGuid}", "${serviceGuid}"), "productId"))
    .exec(ApiCalendar.save(ApiCalendar.saveJson("Calendar ${calendarGuid}"), "calendarId"))
    .exec(ApiCalendar.calendarProperties(ApiCalendar.calendarPropertiesJson("peak", "13:00:00", "16:59:59"), "peakCalendarProperty"))
    .exec(ApiCalendar.calendarProperties(ApiCalendar.calendarPropertiesJson("off-peak", "00:00:00", "12:59:59"), "offPeakCalendarProperty"))
    .exec(ApiCalendar.calendarProperties(ApiCalendar.calendarPropertiesJson("prime", "17:00:00", "23:59:59"), "primeCalendarProperty"))
    .exec(ApiRatingParameter.save(ApiRatingParameter.saveJson("location"), "ratingParameterLocation"))
    .exec(ApiRatingParameter.save(ApiRatingParameter.saveJson("destination"), "ratingParameterDestination"))
    .exec(ApiRatingParameterSet.save(ApiRatingParameterSet.saveJson("ParamSet01"), "ratingParameterSetId"))
    .exec(ApiLayout.save(ApiLayout.saveJson("Layout01"), "layoutId"))
    .exec(ApiTariff.save(ApiTariff.saveJson("sms ${tariffGuid}"), "tariffId"))
    .exec(ApiTariffPlan.save(ApiTariffPlan.saveJson("sms tariff plan ${tariffPlanGuid}"), "tariffPlanId"))
    .exec(ApiBalance.save(ApiBalance.saveJson(), "balanceId"))
    .exec(ApiProductOffering.save(ApiProductOffering.saveJson("productOffering ${productOfferingGuid}"), "productOfferingId"))
    // Prepare subscriber infrastructure
    .exec(ApiCustomerAccount.save(ApiCustomerAccount.saveJson(), "customerAccountId"))
    .exec(ApiSubscriber.save(ApiSubscriber.saveJson(), "subscriberId"))
    .exec(ApiLookup.save(ApiLookup.saveJson()))
    .exec(ApiSubscriber.subscriberOffer(ApiSubscriber.subscriberOfferJson("SMS"), "subscriberBalanceId"))
    .exec(ApiTopUp.topUp(ApiTopUp.topUpJson(config.floRaterAccountService.topUp.amount, config.floRaterAccountService.topUp.currency)))
    .exec { session =>
      val subscriberNumber = subscriberReadyCount.incrementAndGet()
      log.info("~~~~~ subscriber #{} / {} is ready", subscriberNumber, config.loadProfile.rampUsers)
      session
    }
    .exec { session =>
      ApiEvents.events(ApiEvents.eventsJson())
      sentEventsCount.incrementAndGet()
      log.info("~~~~~ sentEventsCount: {}", sentEventsCount.get())
      session
    }
    .exec(pause((config.loadProfile.rampSeconds).seconds))
    // send events
    .forever {
      asLongAs(condition = _ => allSubscribersAreReady(), exitASAP = false) {
        pace(config.loadProfile.paceMilliseconds.toString, unit = TimeUnit.MILLISECONDS)
          .exec(ApiEvents.events(ApiEvents.eventsJson()))
          .exec { session =>
            val count = sentEventsCount.incrementAndGet()
            val status = session("eventsHttpResponseCode").as[Int]
            if (200 != status) {
              log.warn("~~~~~ event #{} failed with status:{}", count, status)
            }
            session
          }
      }
    }


  setUp(
    scnPrepareInfrastructureAndSendEvents.inject(rampUsers(config.loadProfile.rampUsers) during config.loadProfile.rampSeconds))
    .protocols(floRaterProtocol)
    .maxDuration(config.loadProfile.maxDurationSeconds)
    .assertions(
      forAll.failedRequests.count.is(0),
    )

  before {
    log.info("before: ~~~~~  sentEventsCount: {}", sentEventsCount.get())
  }

  after {
    log.info("after:  ~~~~~  sentEventsCount: {}", sentEventsCount.get())
  }
}