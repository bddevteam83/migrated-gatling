package load

import load.api.{ApiPriceItem, PagedResourceMethods}

class PriceItemSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiPriceItem

}