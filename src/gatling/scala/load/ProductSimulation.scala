package load

import load.api.{ApiProduct, PagedResourceMethods}

class ProductSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiProduct

}