package load

import load.api.{ApiSubscriber, PagedResourceMethods}

class SubscriberSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiSubscriber

}