package load

import load.api.{ApiLookup, PagedResourceMethods}

class LookupSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiLookup

}