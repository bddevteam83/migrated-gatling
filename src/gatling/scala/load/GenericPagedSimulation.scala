package load

import io.gatling.core.Predef._
import load.api.PagedResourceMethods
import load.base._

import java.util.concurrent.TimeUnit

trait GenericPagedSimulation extends Simulation with Log with Config {

  log.info("~~~~~ config: {}", config)

  def pagedResource(): PagedResourceMethods

  val resourceName = pagedResource().resourceName()

  val feederIds = Iterator.continually {
    Map(s"${resourceName}Id" -> pagedResource().getNextId())
  }

  val scnGetAll = scenario(s"${resourceName}.getAll").repeat(1, "i") {
    exec(pagedResource().getAll())
  }

  val scnGetById = scenario(s"${resourceName}.getById")
    .asLongAs(condition = _ => pagedResource().fetchedIds.nonEmpty, exitASAP = false) {
      pace(config.loadProfile.paceMilliseconds.toString, unit = TimeUnit.MILLISECONDS)
        .feed(feederIds)
        .exec(pagedResource().getById(s"$${${resourceName}Id}"))
    }

  setUp(
    scnGetAll.inject(atOnceUsers(1)),
    scnGetById.inject(rampUsers(config.loadProfile.rampUsers) during (config.loadProfile.rampSeconds))
  )
    .protocols(pagedResource().floRaterProtocol)
    .maxDuration(config.loadProfile.maxDurationSeconds)
    .assertions(
      forAll.failedRequests.count.is(0),
      details("getById").responseTime.percentile4.lte(config.loadProfile.expectedResponseTimeMsec),
    )

}