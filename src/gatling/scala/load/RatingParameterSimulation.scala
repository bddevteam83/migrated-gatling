package load

import load.api.{ApiRatingParameter, PagedResourceMethods}

class RatingParameterSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiRatingParameter

}