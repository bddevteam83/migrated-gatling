package load

import load.api.{ApiCurrency, PagedResourceMethods}

class CurrencySimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiCurrency

}