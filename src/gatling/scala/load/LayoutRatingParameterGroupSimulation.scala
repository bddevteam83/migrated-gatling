package load

import load.api.{ApiLayoutRatingParameterGroup, PagedResourceMethods}

class LayoutRatingParameterGroupSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiLayoutRatingParameterGroup

}