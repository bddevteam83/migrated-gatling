package load

import load.api.{ApiActionTrigger, PagedResourceMethods}

class ActionTriggerSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiActionTrigger

}