package load

import load.api.{ApiCalendar, PagedResourceMethods}

class CalendarSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiCalendar


}