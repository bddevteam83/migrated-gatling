package load

import io.gatling.core.Predef._
import load.api._
import load.base._

import java.util.UUID

/**
 * based on com.flo.rater.test.floconnect.FloConnectHierarchyInternetScenario integration test
 */
class SendEvents3Simulation extends Simulation with Log with Config with FloRaterProtocol with UtcTimeZone {

  log.info("~~~~~ config: {}", config)

  def guid() = UUID.randomUUID().toString

  val feederInit = Iterator.continually {
    Map(
      // Prepare catalog infrastructure
      "serviceGuid" -> guid(),
      "providerGuid" -> guid(),
      "productGuid" -> guid(),
      "calendarGuid" -> guid(),
      "tariffGuid" -> guid(),
      "tariffPlanGuid" -> guid(),
      "tariffPlanRange1Guid" -> guid(),
      "tariffPlanRange2Guid" -> guid(),
      "tariffPlanRange3Guid" -> guid(),
      "balanceGuid" -> guid(),
      "productOfferingGuid" -> guid(),
      "productOfferingPriceGuid" -> guid(),
      // Prepare subscriber infrastructure
      "firstCustomerGuid" -> guid(),
      "firstCustomerAccountGuid" -> guid(),
      "secondCustomerGuid" -> guid(),
      "secondCustomerAccountGuid" -> guid(),
      "thirdCustomerGuid" -> guid(),
      "thirdCustomerAccountGuid" -> guid(),

      "subscriberGuId" -> guid(),
      "externalTransactionGuId" -> guid(),
      // rating event
      "externalSessionGuId" -> guid()
    )
  }

  val scnSubscriberHierarchy = scenario("Subscriber hierarchy")
    .feed(feederInit)

    .exec(ApiCustomerAccount.save(ApiCustomerAccount.saveJson(customerAccountGuid = "${firstCustomerAccountGuid}", customerGuid = "${firstCustomerGuid}"), "firstLevelAccountId", "firstLevelCustomerId"))
    .exec(ApiSubscriber.getVirtualSubscriberIdByCustomerAccountId("${firstCustomerAccountGuid}", "firstLevelVirtualSubscriberId"))
    .exec(ApiSubscriber.save(ApiSubscriber.saveJson(customerAccountId = "${firstLevelAccountId}"), "firstLevelNonVirtualSubscriberId"))

    .exec(ApiCustomerAccount.save(ApiCustomerAccount.saveJsonWithParent(customerAccountGuid = "${secondCustomerAccountGuid}", customerGuid = "${secondCustomerGuid}", parentId = "${firstLevelCustomerId}"), "secondLevelAccountId", "secondLevelCustomerId"))
    .exec(ApiSubscriber.getVirtualSubscriberIdByCustomerAccountId("${secondCustomerAccountGuid}", "secondLevelVirtualSubscriberId"))

    .exec(ApiCustomerAccount.save(ApiCustomerAccount.saveJsonWithParent(customerAccountGuid = "${thirdCustomerAccountGuid}", customerGuid = "${thirdCustomerGuid}", parentId = "${secondLevelCustomerId}"), "thirdLevelAccountId", "thirdLevelCustomerId"))
    .exec(ApiSubscriber.getVirtualSubscriberIdByCustomerAccountId("${thirdCustomerAccountGuid}", "thirdLevelVirtualSubscriberId"))
    //
    .exec(ApiSubscriber.getById("${firstLevelNonVirtualSubscriberId}", "getById1"))
    .exec { session =>
      val secondLevelAccountId = session("secondLevelAccountId").as[String]
      val upd1: String = session("getById1").as[String].replaceAll("\"customerAccountId\":\"[0-9a-z-]+\"", "\"customerAccountId\":\"" + secondLevelAccountId + "\"")
      session.set("update1", upd1)
    }
    .exec(ApiSubscriber.update("${firstLevelNonVirtualSubscriberId}", "${update1}", "secondLevelMovedNonVirtualSubscriberId"))
    //
    .exec(ApiSubscriber.getById("${secondLevelMovedNonVirtualSubscriberId}", "getById2"))
    .exec { session =>
      val thirdLevelAccountId = session("thirdLevelAccountId").as[String]
      val upd2: String = session("getById2").as[String].replaceAll("\"customerAccountId\":\"[0-9a-z-]+\"", "\"customerAccountId\":\"" + thirdLevelAccountId + "\"")
      session.set("update2", upd2)
    }
    .exec(ApiSubscriber.update("${secondLevelMovedNonVirtualSubscriberId}", "${update2}", "thirdLevelMovedNonVirtualSubscriberId"))
    //
    .exec(ApiSubscriber.getById("${secondLevelMovedNonVirtualSubscriberId}", "getById11"))
    .exec(ApiSubscriber.getById("${thirdLevelMovedNonVirtualSubscriberId}", "getById22"))


  /*
          val secondaryLevelMovedNonVirtualSubscriber = apiControllers.subscriberApiController.moveSubscriber(firstLevelNonVirtualSubscriber.id!!, secondLevelAccount.id)
          val thirdLevelMovedNonVirtualSubscriber = apiControllers.subscriberApiController.moveSubscriber(secondaryLevelMovedNonVirtualSubscriber.id!!, thirdLevelAccount.id)

   */

  setUp(
    scnSubscriberHierarchy.inject(atOnceUsers(1)))
    .protocols(floRaterProtocol)
    .maxDuration(config.loadProfile.maxDurationSeconds)
    .assertions(
      forAll.failedRequests.count.is(0),
    )

}