package load

import load.api.{ApiTariffPlan, PagedResourceMethods}

class TariffPlanSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiTariffPlan

}