package load

import io.gatling.core.Predef._
import load.api._
import load.base._

import java.util.UUID
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong
import scala.concurrent.duration.DurationInt

/**
 * Init data procedure is based on postman collection provided by Heorhii Zhukov
 * https://www.getpostman.com/collections/a034386997b76c5f5ba1
 */
class SendEventsSimulation extends Simulation with Log with Config with FloRaterProtocol with UtcTimeZone {

  log.info("~~~~~ config: {}", config)

  val sentEventsCount = new AtomicLong(0);
  var serviceName: String = null
  var subscriberId: String = null
  var subscriberBalanceId: String = null

  def isReadyToSendEvents(): Boolean = (null != serviceName) && (null != subscriberId) && (null != subscriberBalanceId)

  def guid() = UUID.randomUUID().toString

  val feederInit = Iterator.continually {
    Map(
      // Prepare catalog infrastructure
      "serviceGuid" -> guid(),
      "providerGuid" -> guid(),
      "productGuid" -> guid(),
      "calendarGuid" -> guid(),
      "tariffGuid" -> guid(),
      "tariffPlanGuid" -> guid(),
      "tariffPlanRange1Guid" -> guid(),
      "tariffPlanRange2Guid" -> guid(),
      "tariffPlanRange3Guid" -> guid(),
      "balanceGuid" -> guid(),
      "productOfferingGuid" -> guid(),
      "productOfferingPriceGuid" -> guid(),
      // Prepare subscriber infrastructure
      "customerAccountGuid" -> guid(),
      "subscriberGuId" -> guid(),
      "customerGuid" -> guid(),
      "externalTransactionGuId" -> guid(),
      // rating event
      "externalSessionGuId" -> guid()
    )
  }
  val feederEvent = Iterator.continually {
    Map(
      // rating event
      "externalSessionGuId" -> guid(),
      "serviceName" -> serviceName,
      "subscriberId" -> subscriberId,
      "subscriberBalanceId" -> subscriberBalanceId
    )
  }

  val scnPrepareInfrastructure = scenario("Prepare catalog and subscriber infrastructure")
    .feed(feederInit)
    // Prepare catalog infrastructure
    .exec(ApiService.save(ApiService.saveJson("SMS ${serviceGuid}"), "serviceName"))
    .exec(ApiProvider.save(ApiProvider.saveJson("Provider ${providerGuid}", "${serviceGuid}"), "serviceId"))
    .exec(ApiProduct.save(ApiProduct.saveJson("Product ${productGuid}", "${serviceGuid}"), "productId"))
    .exec(ApiCalendar.save(ApiCalendar.saveJson("Calendar ${calendarGuid}"), "calendarId"))
    .exec(ApiCalendar.calendarProperties(ApiCalendar.calendarPropertiesJson("peak", "13:00:00", "16:59:59"), "peakCalendarProperty"))
    .exec(ApiCalendar.calendarProperties(ApiCalendar.calendarPropertiesJson("off-peak", "00:00:00", "12:59:59"), "offPeakCalendarProperty"))
    .exec(ApiCalendar.calendarProperties(ApiCalendar.calendarPropertiesJson("prime", "17:00:00", "23:59:59"), "primeCalendarProperty"))
    .exec(ApiRatingParameter.save(ApiRatingParameter.saveJson("location"), "ratingParameterLocation"))
    .exec(ApiRatingParameter.save(ApiRatingParameter.saveJson("destination"), "ratingParameterDestination"))
    .exec(ApiRatingParameterSet.save(ApiRatingParameterSet.saveJson("ParamSet01"), "ratingParameterSetId"))
    .exec(ApiLayout.save(ApiLayout.saveJson("Layout01"), "layoutId"))
    .exec(ApiTariff.save(ApiTariff.saveJson("sms ${tariffGuid}"), "tariffId"))
    .exec(ApiTariffPlan.save(ApiTariffPlan.saveJson("sms tariff plan ${tariffPlanGuid}"), "tariffPlanId"))
    .exec(ApiBalance.save(ApiBalance.saveJson(), "balanceId"))
    .exec(ApiProductOffering.save(ApiProductOffering.saveJson("productOffering ${productOfferingGuid}"), "productOfferingId"))
    // Prepare subscriber infrastructure
    .exec(ApiCustomerAccount.save(ApiCustomerAccount.saveJson(), "customerAccountId"))
    .exec(ApiSubscriber.save(ApiSubscriber.saveJson(), "subscriberId"))
    .exec(ApiLookup.save(ApiLookup.saveJson()))
    .exec(ApiSubscriber.subscriberOffer(ApiSubscriber.subscriberOfferJson("SMS"), "subscriberBalanceId"))
    .exec(ApiTopUp.topUp(ApiTopUp.topUpJson(config.floRaterAccountService.topUp.amount, config.floRaterAccountService.topUp.currency)))
    .exec(pause(1.seconds))
    .exec { session =>
      serviceName = session("serviceName").as[String]
      subscriberId = session("subscriberId").as[String]
      subscriberBalanceId = session("subscriberBalanceId").as[String]
      log.info("~~~~~~~~~~ init done: serviceName:{} subscriberId:{} subscriberBalanceId:{}", serviceName, subscriberId, subscriberBalanceId)

      session
    }
  //.exec(ApiEvents.events(ApiEvents.eventsJson()))
  //    .repeat(1000, "i") {
  //      pace(config.loadProfile.paceMilliseconds.toString, unit = TimeUnit.MILLISECONDS).
  //        exec(ApiEvents.events(ApiEvents.eventsJson()))
  //    }

  val scnSendEvent = scenario("Send events")
    .asLongAs(condition = _ => isReadyToSendEvents(), exitASAP = false) {
      pace(config.loadProfile.paceMilliseconds.toString, unit = TimeUnit.MILLISECONDS)
        .feed(feederEvent)
        .exec(ApiEvents.events(ApiEvents.eventsJson()))
        .exec { session =>
          val count = sentEventsCount.incrementAndGet()
          val status = session("eventsHttpResponseCode").as[Int]
          if (200 != status) {
            log.warn("~~~~~ event #{} failed with status:{}", count, status)
          }
          session
        }
    }

  setUp(
    scnPrepareInfrastructure.inject(atOnceUsers(1)).andThen(
      scnSendEvent.inject(rampUsers(config.loadProfile.rampUsers) during (config.loadProfile.rampSeconds))
    )
  )
    .protocols(floRaterProtocol)
    .maxDuration(config.loadProfile.maxDurationSeconds)
    .assertions(
      forAll.failedRequests.count.is(0),
    )

  before {
    log.info("before: ~~~~~  sentEventsCount: {}", sentEventsCount.get())
  }

  after {
    log.info("after:  ~~~~~  sentEventsCount: {}", sentEventsCount.get())
  }
}