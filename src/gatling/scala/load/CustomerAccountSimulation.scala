package load

import load.api.{ApiCustomerAccount, PagedResourceMethods}

class CustomerAccountSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiCustomerAccount

}