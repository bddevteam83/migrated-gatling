package load.base

import com.typesafe.config.{ConfigBeanFactory, ConfigFactory}

import scala.beans.BeanProperty

trait Config {
  val config = ConfigBeanFactory.create(ConfigFactory.load(), classOf[TestConfig]);
}

class TestConfig {
  @BeanProperty var loadProfile: LoadProfile = null
  @BeanProperty var pongService: PongService = null
  @BeanProperty var floRaterEngineService: FloRaterEngineService = null
  @BeanProperty var floRaterAccountService: FloRaterAccountService = null

  override def toString = s"TestConfig(loadProfile=$loadProfile, pongService=$pongService, floRaterService=$floRaterAccountService)"
}

class LoadProfile {
  @BeanProperty var paceMilliseconds = 1
  @BeanProperty var rampUsers = 10
  @BeanProperty var rampSeconds = 10
  @BeanProperty var maxDurationSeconds = 20
  @BeanProperty var expectedResponseTimeMsec = 4000

  override def toString = s"LoadProfile(paceMilliseconds=$paceMilliseconds, rampUsers=$rampUsers, rampSeconds=$rampSeconds, maxDurationSeconds=$maxDurationSeconds, expectedResponseTimeMsec=$expectedResponseTimeMsec)"
}

class PongService {
  @BeanProperty var url = "http://localhost:8100"

  override def toString = s"PongService(url=$url)"
}

class FloRaterEngineService {
  @BeanProperty var url = "http://10.0.9.100:8080"

  override def toString = s"FloRaterEngineService(url=$url)"
}

class FloRaterAccountService {
  @BeanProperty var url = "http://10.0.9.100:8080"
  @BeanProperty var maxPageSize = 100
  @BeanProperty var topUp: TopUp = null

  override def toString = s"FloRaterAccountService(url=$url, maxPageSize=$maxPageSize, topUp=$topUp)"
}

class TopUp {
  @BeanProperty var amount = 100000
  @BeanProperty var currency = "USD"

  override def toString = s"TopUp(amount=$amount, currency=$currency)"
}