package load.base

import java.util.TimeZone


trait UtcTimeZone {


  TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

}
