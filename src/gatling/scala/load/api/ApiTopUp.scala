package load.api

import io.gatling.core.Predef.{exec, _}
import io.gatling.http.Predef.{http, status, _}

object ApiTopUp {

  def resourceName() = "top-up"

  def topUp(body: String, saveTransactionIdAs: String = s"${resourceName()}-status") = exec(
    http(s"${resourceName()}.save")
      .post(s"/${resourceName()}")
      .body(StringBody(body)).asJson
      .check(status.is(200))
      .check(jsonPath("$[?(@.status=='OK')].transactionDetails.transactionId").ofType[String].find.saveAs(saveTransactionIdAs))
  )

  def topUpJson(amount: Int = 100000, currency: String = "USD") =
    s"""{
       |    "initiatorIdentifier": "$${subscriberId}",
       |    "subscriberBalanceId": "$${subscriberBalanceId}",
       |    "amount": "${amount}",
       |    "currency": "${currency}",
       |    "tax": "some info about taxes",
       |    "externalTransactionId": "$${externalTransactionGuId}",
       |    "externalTransactionDate" : null,
       |    "expirationDate": null,
       |    "expirationType": null,
       |    "customFields": null
       |}""".stripMargin
}
