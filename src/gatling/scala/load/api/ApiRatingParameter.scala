package load.api

object ApiRatingParameter extends PagedResourceMethods with SaveMethod {

  def resourceName() = "rating-parameters"

  def saveJson(name: String): String =
    s"""{
       |  "name": "${name}",
       |  "realm": ""
       |}""".stripMargin
}
