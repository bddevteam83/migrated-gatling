package load.api

object ApiProvider extends PagedResourceMethods with SaveMethod {

  def resourceName() = "providers"

  def saveJson(name: String, serviceId: String): String =
    s"""{
       |    "id": "$${providerGuid}",
       |    "name": "${name}",
       |    "realm": "",
       |    "codes": [
       |        "$${providerGuid}"
       |    ],
       |    "isDefault": false,
       |    "serviceIds": [
       |        "${serviceId}"
       |    ]
       |}""".stripMargin
}
