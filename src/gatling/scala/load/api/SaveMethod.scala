package load.api

import io.gatling.core.Predef.{exec, _}
import io.gatling.http.Predef.{http, status, _}
import load.base.{Config, Log}

trait SaveMethod extends Log with Config with FloRaterProtocol {

  def resourceName(): String

  def save(body: String, saveIdAs: String = s"${resourceName()}-id") = exec(
    http(s"${resourceName()}.save")
      .post(s"/${resourceName()}")
      .body(StringBody(body)).asJson
      .check(status.is(200))
      .check(jsonPath("$.id").ofType[String].find.saveAs(saveIdAs))
  )

}
