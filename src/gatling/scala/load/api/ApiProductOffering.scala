package load.api

object ApiProductOffering extends PagedResourceMethods with SaveMethod {

  def resourceName() = "product-offerings"

  def saveJson(name: String): String =
    s"""{
       |  "description": "description",
       |  "id": "$${productOfferingGuid}",
       |  "isDeleted": false,
       |  "isPrimary": true,
       |  "isOffered": false,
       |  "isIndividual": false,
       |  "name": "${name}",
       |  "cardinality": 5,
       |  "price": {
       |        "currencyId": "USD",
       |        "id": "$${productOfferingPriceGuid}",
       |        "realm": "",
       |        "value": 10
       |  },
       |  "productId": "$${productId}",
       |  "realm": "",
       |  "nestedProductOfferIds": [],
       |  "type": "SIMPLE_PRODUCT_OFFERING",
       |  "usageBalanceId": "$${balanceId}",
       |  "periodicBalanceId": "$${balanceId}",
       |  "offerTerm": {
       |    "allowance": 0,
       |    "allowanceRenewalStrategy": "OVERRIDE",
       |    "effectiveDateFrom": "2021-06-07T21:00:00Z",
       |    "effectiveDateTo": null,
       |    "realm": "",
       |    "offerPriority": 90,
       |    "recurringType": {
       |      "realm": "",
       |      "type": "ONE_TIME",
       |      "value": 0,
       |      "isProrated": false,
       |      "isCalendar": true
       |    },
       |    "tariffPlanId": "$${tariffPlanId}",
       |    "currencyId": "USD",
       |    "offerRenewalStrategy": "REGULAR",
       |    "type": "RATING"
       |  },
       |  "limit": null,
       |  "poolPlan": null,
       |  "eligibilitySetIds": [],
       |  "compatibilitySetIds": [],
       |  "customFields": {"gatling":"test"}
       |}""".stripMargin
}
