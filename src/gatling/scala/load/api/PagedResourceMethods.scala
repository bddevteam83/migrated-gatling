package load.api

import io.gatling.core.Predef.{exec, jsonPath, _}
import io.gatling.http.Predef.{http, status, _}
import load.base.{Config, Log}

import java.util.concurrent.atomic.AtomicInteger
import scala.collection.mutable.ListBuffer

trait PagedResourceMethods extends Log with Config with FloRaterProtocol {

  def resourceName(): String

  def maxPageSize() = config.floRaterAccountService.maxPageSize

  val totalElements = "totalElements"
  val elementIds = "elementIds"
  val search = ""
  val sort = "id,asc"

  var fetchedIds: ListBuffer[String] = new ListBuffer()
  val idx = new AtomicInteger(0)

  def getNextId(): String = {
    fetchedIds(idx.getAndIncrement() % fetchedIds.size)
  }

  def countTotal() = exec(httpGetAllAndSaveTotal("0", "1"))

  def getAll() =
    exec(httpGetAllAndSaveTotal("0", "1"))
      .exec { session => fetchedIds = new ListBuffer(); session }
      .repeat(session => 1 + (session(totalElements).as[Int] / maxPageSize()), "page") {
        exec()
          .exec(httpGetAllAndSaveIds("${page}", maxPageSize().toString))
          .exec { session => fetchedIds.addAll(session(elementIds).as[Seq[String]]); session }
      }.exec { session => log.info("fetchedIds.size: {}", fetchedIds.size); session }


  def httpGetAll(page: String, size: String, search: String = search, sort: String = sort)
  = http("getAll")
    .get(s"/${resourceName()}")
    .queryParam("page", page)
    .queryParam("search", search)
    .queryParam("size", size)
    .queryParam("sort", sort)
    .check(status.is(200))

  def httpGetAllAndSaveTotal(page: String, size: String)
  = httpGetAll(page, size)
    .check(jsonPath("$.totalElements").ofType[Int].saveAs(totalElements))

  def httpGetAllAndSaveIds(page: String, size: String)
  = httpGetAll(page, size)
    .check(jsonPath("$.content[*].id").ofType[String].findAll.saveAs(elementIds))

  def getById(id: String, saveResponseAs: String = s"${resourceName()}-body") = exec(
    http("getById")
      .get(s"/${resourceName()}/${id}")
      .check(status.is(200))
      .check(bodyString.saveAs(saveResponseAs))
  )

}
