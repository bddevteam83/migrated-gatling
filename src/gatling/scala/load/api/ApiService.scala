package load.api

import io.gatling.core.Predef.{exec, _}
import io.gatling.http.Predef.{http, status, _}

object ApiService extends PagedResourceMethods with SaveMethod {

  def resourceName() = "services"

  override def save(body: String, saveNameAs: String = s"${resourceName()}-name") = exec(
    http(s"${resourceName()}.save")
      .post(s"/${resourceName()}")
      .body(StringBody(body)).asJson
      .check(status.is(200))
      .check(jsonPath("$.name").ofType[String].find.saveAs(saveNameAs))
  )

  def saveJson(name: String): String =
    s"""{
       |  "id": "$${serviceGuid}",
       |  "name": "${name}",
       |  "providerIds": [],
       |  "realm": "",
       |  "statusIds": [
       |    "status"
       |  ]
       |}""".stripMargin
}
