package load.api

import io.gatling.core.Predef.{StringBody, exec, jsonPath, _}
import io.gatling.http.Predef.{http, status, _}

object ApiCustomerAccount extends PagedResourceMethods with SaveMethod {

  def resourceName() = "customer-accounts"

  def save(body: String, saveIdAs: String, saveCustomerIdAs: String) = exec(
    http(s"${resourceName()}.save")
      .post(s"/${resourceName()}")
      .body(StringBody(body)).asJson
      .check(status.is(200))
      .check(jsonPath("$.id").ofType[String].find.saveAs(saveIdAs))
      .check(jsonPath("$.customer.id").ofType[String].find.saveAs(saveCustomerIdAs))
  )

  def saveJson(customerAccountGuid: String = "${customerAccountGuid}", customerGuid: String = "${customerGuid}"): String =
    s"""{
       |    "id": "${customerAccountGuid}",
       |    "realm": "",
       |    "version": 0,
       |    "status": "string",
       |    "path": "",
       |    "currencyId": "USD",
       |    "parentId": null,
       |    "customer": {
       |        "id": "${customerGuid}",
       |        "parentId": null,
       |        "path": "string",
       |        "realm": "",
       |        "status": "string"
       |    }
       |}""".stripMargin

  def saveJsonWithParent(customerAccountGuid: String = "${customerAccountGuid}", customerGuid: String = "${customerGuid}", parentId: String): String =
    s"""{
       |    "id": "${customerAccountGuid}",
       |    "realm": "",
       |    "version": 0,
       |    "status": "string",
       |    "path": "",
       |    "currencyId": "USD",
       |    "parentId": null,
       |    "customer": {
       |        "id": "${customerGuid}",
       |        "parentId": "${parentId}",
       |        "path": "string",
       |        "realm": "",
       |        "status": "string"
       |    }
       |}""".stripMargin
}
