package load.api

import io.gatling.core.Predef.{StringBody, exec, _}
import io.gatling.http.Predef.{http, status, _}
import load.base.Config

import java.text.SimpleDateFormat
import java.util.Date


object ApiEvents extends Object with Config {

  def resourceName() = "events"

  def events(body: String, saveIdAs: String = s"${resourceName()}-id") = exec(
    http(s"${resourceName()}")
      .post(s"${config.floRaterEngineService.url}/${resourceName()}")
      .queryParam("offline", false)
      .body(StringBody(body)).asJson
      .check(status.saveAs("eventsHttpResponseCode"))
      .check(status.is(200))
    //      .check(jsonPath("$.id").ofType[String].find.saveAs(saveIdAs))
  )


  // 2020-08-07T13:30:00.289+03:00[UTC]
  // 2021-03-31T14:40:47.528+03:00[UTC]
  def originateTime(date: Date): String = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").format(date) + "[UTC]"


  def eventsJson() =
    s"""{
       |    "realm": "",
       |    "serviceName": "$${serviceName}",
       |    "ratingParameters": [
       |        "+38063",
       |        "+38044"
       |    ],
       |    "initiatorIdentifier": "$${subscriberId}",
       |    "originateTime": "$${currentDate(yyyy-MM-dd'T'HH:mm:ss.SSSXXX)}[UTC]",
       |    "externalSessionId": "$${externalSessionGuId}",
       |    "direction": null,
       |    "cdrFileId": null,
       |    "customFields": null,
       |    "quota": null,
       |    "quotaUnit": null,
       |    "sessionId": null,
       |    "eventType": "STOP"
       |}""".stripMargin

}
