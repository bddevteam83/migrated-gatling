package load.api

object ApiBalance extends PagedResourceMethods with SaveMethod {

  def resourceName() = "balances"

  def saveJson(): String =
    s"""{
       |  "id": "$${balanceGuid}",
       |  "realm": "",
       |  "type": "PREPAID",
       |  "currencyId": "USD",
       |  "minimumBalance": -1000,
       |  "maximumBalance": 100000
       |}""".stripMargin

}
