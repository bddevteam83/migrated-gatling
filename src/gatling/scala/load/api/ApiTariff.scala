package load.api

object ApiTariff extends PagedResourceMethods with SaveMethod {

  def resourceName() = "tariffs"

  def saveJson(name: String): String =
    s"""{
       |    "name": "${name}",
       |    "realm": "",
       |    "type": "REGULAR",
       |    "version": 0,
       |    "steps": [
       |        {
       |            "name": "string",
       |            "nextStep": null,
       |            "priceItem": {
       |                "currencyId": "USD",
       |                "description": "string",
       |                "name": "${name}",
       |                "realm": "",
       |                "value": 5,
       |                "version": 0
       |            },
       |            "value": 5,
       |            "repetitions": null,
       |            "step": 1,
       |            "version": 0,
       |            "currencyId": "SMS"
       |        }
       |    ]
       |}""".stripMargin

}
