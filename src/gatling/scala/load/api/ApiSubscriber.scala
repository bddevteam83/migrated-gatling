package load.api

import io.gatling.core.Predef.{exec, _}
import io.gatling.http.Predef.{http, status, _}

object ApiSubscriber extends PagedResourceMethods with SaveMethod with UpdateMethod {

  def resourceName() = "subscribers"

  def saveJson(customerAccountId: String = "${customerAccountId}"): String =
    s"""{
       |  "id": "$${subscriberGuId}",
       |  "version": 0,
       |  "isVirtual": false,
       |  "status": "string",
       |  "customerAccountId": "${customerAccountId}"
       |}""".stripMargin


  def subscriberOffer(body: String, saveIdAs: String = s"${resourceName()}-offer-id") = exec(
    http("subscriberOffer")
      .post(s"/${resourceName()}/$${subscriberId}/offers")
      .body(StringBody(body)).asJson
      .check(status.is(200))
      //      .check(jsonPath("$.id").ofType[String].find.saveAs(saveIdAs))
      .check(jsonPath("$.balanceAggregators[?(@.balanceType == 'USAGE')].balanceId").ofType[String].find.saveAs(saveIdAs))

  )

  def subscriberOfferJson(name: String) =
    s"""{
       |    "description": "string",
       |    "name": "${name}",
       |    "priority": 0,
       |    "productOfferingId": "$${productOfferingId}",
       |    "realm": "",
       |    "subscriberId": "$${subscriberId}"
       |}""".stripMargin


  def getVirtualSubscriberIdByCustomerAccountId(uuid: String, saveIdAs: String) = httpGetAll(page = "0", size = "1", search = s"customerAccountId==${uuid}")
    .check(status.is(200))
    .check(jsonPath("$.content[0].isVirtual").is("true"))
    .check(jsonPath("$.content[0].id").ofType[String].saveAs(saveIdAs))

}
