package load.api

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import load.base.Config

trait FloRaterProtocol extends Config {

  val floRaterProtocol = http
    .baseUrl(config.floRaterAccountService.url)
    .userAgentHeader("Gatling Performance Test")
    .shareConnections

}
