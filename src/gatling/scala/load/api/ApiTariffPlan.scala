package load.api

object ApiTariffPlan extends PagedResourceMethods with SaveMethod {

  def resourceName() = "tariff-plans"

  def saveJson(name: String): String =
    s"""{
       |  "calendarId": "$${calendarId}",
       |  "description": "description",
       |  "id": "$${tariffPlanGuid}",
       |  "layoutId": "$${layoutId}",
       |  "name": "${name}",
       |  "realm": "",
       |  "version": 0,
       |
       |  "isIndividual": false,
       |  "createdDate": "$${currentDate(yyyy-MM-dd'T'HH:mm:ss.SSSXXX)}",
       |  "updatedDate": null,
       |  "isDeleted": false,
       |
       |  "ranges": [
       |    {
       |      "tariffId": "$${tariffId}",
       |      "list": [
       |        {
       |          "id": "$${tariffPlanRange1Guid}",
       |          "position": 0,
       |          "calendarPropertiesId": "$${peakCalendarProperty}"
       |        },
       |        {
       |          "id": "$${tariffPlanRange2Guid}",
       |          "position": 1,
       |          "calendarPropertiesId": "$${offPeakCalendarProperty}"
       |        },
       |        {
       |          "id": "$${tariffPlanRange3Guid}",
       |          "position": 2,
       |          "calendarPropertiesId": "$${primeCalendarProperty}"
       |        }
       |      ]
       |    }
       |  ]
       |}""".stripMargin

}
