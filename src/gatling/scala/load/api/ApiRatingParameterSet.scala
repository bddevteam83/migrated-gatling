package load.api

object ApiRatingParameterSet extends PagedResourceMethods with SaveMethod {

  def resourceName() = "rating-parameter-sets"

  def saveJson(name: String): String =
    s"""{
       |    "name": "${name}",
       |    "ratingParameterIds": [
       |        "$${ratingParameterLocation}",
       |        "$${ratingParameterDestination}"
       |    ],
       |    "realm": ""
       |}""".stripMargin

}
