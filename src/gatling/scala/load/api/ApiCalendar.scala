package load.api

import io.gatling.core.Predef.{exec, _}
import io.gatling.http.Predef.{http, status, _}

object ApiCalendar extends PagedResourceMethods with SaveMethod {

  def resourceName() = "calendars"

  def saveJson(name: String): String =
    s"""{
       |  "description": "calendar description",
       |  "id": "$${calendarGuid}",
       |  "name": "${name}",
       |  "realm": "",
       |  "version": 0
       |}""".stripMargin


  def calendarProperties(body: String, saveIdAs: String = s"${resourceName()}-property-id") = exec(
    http("calendarProperties")
      .post(s"/${resourceName()}/$${calendarGuid}/properties")
      .body(StringBody(body)).asJson
      .check(status.is(200))
      .check(jsonPath("$.id").ofType[String].find.saveAs(saveIdAs))
  )

  def calendarPropertiesJson(name: String, timeFrom: String, timeTo: String) =
    s"""{
       |    "catalogCalendarId": "$${calendarGuid}",
       |    "domFrom": 0,
       |    "domTo": null,
       |    "isDeleted": false,
       |    "monthFrom": 1,
       |    "monthTo": 12,
       |    "name": "${name}",
       |    "timeFrom": "${timeFrom}",
       |    "timeTo": "${timeTo}",
       |    "version": 0
       |}""".stripMargin

}
