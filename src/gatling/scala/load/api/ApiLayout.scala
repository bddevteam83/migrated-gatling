package load.api

object ApiLayout extends PagedResourceMethods with SaveMethod {

  def resourceName() = "layouts"

  def saveJson(name: String): String =
    s"""{
       |       "data": [
       |        {
       |            "parameters": [
       |                 {
       |                    "groupName": null,
       |                    "value": [
       |                        "*"
       |                    ]
       |                },
       |                {
       |                    "groupName": null,
       |                    "value": [
       |                        "*"
       |                    ]
       |                }
       |            ],
       |            "position": 0
       |        }
       |    ],
       |    "name": "${name}",
       |    "ratingParameterSetId": "$${ratingParameterSetId}",
       |    "realm": "",
       |    "version": 0
       |}""".stripMargin
}
