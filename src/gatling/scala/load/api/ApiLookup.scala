package load.api

object ApiLookup extends PagedResourceMethods with SaveMethod {

  def resourceName() = "lookups"

  def saveJson(): String =
    s"""{
       |  "module": "subscriber",
       |  "key": "$${subscriberId}",
       |  "value": "$${subscriberId}",
       |  "realm": ""
       |}""".stripMargin
}
