package load.api

import io.gatling.core.Predef.{exec, _}
import io.gatling.http.Predef.{http, status, _}
import load.base.{Config, Log}

trait UpdateMethod extends Log with Config with FloRaterProtocol {

  def resourceName(): String

  def update(id: String, body: String, saveIdAs: String = s"${resourceName()}-id") = exec(
    http(s"${resourceName()}.update")
      .put(s"/${resourceName()}/${id}")
      .body(StringBody(body)).asJson
      .check(status.is(200))
      .check(jsonPath("$.id").ofType[String].find.saveAs(saveIdAs))
  )

}
