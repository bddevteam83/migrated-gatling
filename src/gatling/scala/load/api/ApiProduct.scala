package load.api

object ApiProduct extends PagedResourceMethods with SaveMethod {

  def resourceName() = "products"

  def saveJson(name: String, serviceId: String): String =
    s"""{
       |  "description": "description",
       |  "id": "$${productGuid}",
       |  "isActive": true,
       |  "name": "${name}",
       |  "realm": "",
       |  "serviceId": "${serviceId}"
       |}""".stripMargin
}
