package load

import load.api.{ApiCompatibilitySet, PagedResourceMethods}

class CompatibilitySetSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiCompatibilitySet

}