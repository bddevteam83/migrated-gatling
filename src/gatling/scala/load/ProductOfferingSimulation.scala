package load

import load.api.{ApiProductOffering, PagedResourceMethods}

class ProductOfferingSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiProductOffering

}