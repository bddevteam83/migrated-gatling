package load

import load.api.{ApiExchangeRate, PagedResourceMethods}

class ExchangeRateSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiExchangeRate

}