package load

import load.api.{ApiTariff, PagedResourceMethods}

class TariffSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiTariff

}