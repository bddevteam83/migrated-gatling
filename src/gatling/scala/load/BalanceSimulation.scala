package load

import load.api.{ApiBalance, PagedResourceMethods}

class BalanceSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiBalance

}