package load

import load.api.{ApiService, PagedResourceMethods}

class ServiceSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiService


}