package load

import load.api.{ApiEligibilitySet, PagedResourceMethods}

class EligibilitySetSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiEligibilitySet

}