package load

import load.api.{ApiLayout, PagedResourceMethods}

class LayoutSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiLayout

}