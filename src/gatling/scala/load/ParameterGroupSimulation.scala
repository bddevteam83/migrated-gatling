package load

import load.api.{ApiParameterGroup, PagedResourceMethods}

class ParameterGroupSimulation extends GenericPagedSimulation {

  def pagedResource(): PagedResourceMethods = ApiParameterGroup

}